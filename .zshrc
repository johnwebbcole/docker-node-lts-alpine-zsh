export TERM="xterm-256color"
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install


# Customise the Powerlevel9k prompts
zsh_docker_container(){
  local dockerid=$(cat /etc/hostname)
  local color='%F{blue}'
  echo -n "%{$color%}\uf308  $dockerid%{%f%}" # \uf230 is docker 
}
POWERLEVEL9K_CUSTOM_DOCKER_CONTAINER="zsh_docker_container"

POWERLEVEL9K_USER_ROOT_FOREGROUND='white'
POWERLEVEL9K_USER_ROOT_BACKGROUND='red'
POWERLEVEL9K_USER_FOREGROUND='green'
POWERLEVEL9K_USER_BACKGROUND='white'

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(
  status
  dir
  vcs
  newline
  time
)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(user custom_docker_container)
POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
source ~/powerlevel9k/powerlevel9k.zsh-theme

alias ls='ls --color'
