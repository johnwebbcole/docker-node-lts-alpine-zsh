FROM node:lts-alpine
LABEL maintainer="John Cole <johnwebbcole@gmail.com>"
RUN apk --update add \
    zsh git zsh-vcs git-zsh-completion zsh-syntax-highlighting

ADD .zshrc /home/root/.zshrc
USER node
RUN git clone https://github.com/bhilburn/powerlevel9k.git ~/powerlevel9k
ADD .zshrc /home/node/.zshrc
RUN mkdir /home/node/.npm-global
RUN npm config set prefix '/home/node/.npm-global'
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
CMD ["zsh"]
