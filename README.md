# johnwebbcole/node-lts-alpine-zsh

I use zsh and the [powerlevel9k] theme a lot. When I generate images or movies of a terminal, I like that the terminal looks nice.

When making snapshots about software installations, it's nice to use a docker image, especially an [alpine] one. The trouble with this is that the docker prompts are vanilla bash.

This project is a docker image that includes zsh and the powerlevel9k theme.

```
docker pull johnwebbcole/node-lts-alpine-zsh
docker run -it --rm johnwebbcole/node-lts-alpine-zsh
```

![node-lts-alpine-zsh video](node-lts-alpine-zsh.mov)

[powerlevel9k]: https://github.com/bhilburn/powerlevel9k
[alpine]: https://alpinelinux.org/
